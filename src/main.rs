//! A simple 3D scene with light shining over a cube sitting on a plane.

mod camera;

use bevy::{prelude::*, window::WindowFocused};
use bevy_atmosphere::prelude::*;

fn main() {
    App::new()
        .insert_resource(Atmosphere::default())
        .add_plugins(DefaultPlugins)
        .add_plugin(camera::FpsCameraPlugin)
        .add_plugin(AtmospherePlugin::default())
        .add_startup_system(setup_scene_system)
        .add_system(daylight_cycle_system)
        .add_system(handle_window_focus_system)
        .run();
}

fn handle_window_focus_system(mut windows: ResMut<Windows>, mut events: EventReader<WindowFocused>, mut query: Query<&mut camera::FpsCamera>) {
    let window = windows.get_primary_mut().unwrap();
    for e in events.iter() {
        window.set_cursor_lock_mode(e.focused);
        window.set_cursor_visibility(!e.focused);
        for mut camera in &mut query {
            camera.active = e.focused;
        }
    }
}

fn daylight_cycle_system(
    mut sky_mat: ResMut<Atmosphere>,
    mut query: Query<(&mut Transform, &mut DirectionalLight), With<Sun>>,
    time: Res<Time>,
) {
    let mut pos = sky_mat.sun_position;
    let t = time.time_since_startup().as_millis() as f32 / 10000.0;
    pos.y = t.sin();
    pos.z = t.cos();
    sky_mat.sun_position = pos;

    if let Some((mut light_trans, mut directional)) = query.single_mut().into() {
        light_trans.rotation = Quat::from_rotation_x(-pos.y.atan2(pos.z));
        directional.illuminance = t.sin().max(0.0).powf(2.0) * 100000.0;
    }
}

#[derive(Component)]
struct Sun;

/// Set up a simple 3D scene
fn setup_scene_system(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // Plane
    commands.spawn_bundle(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Plane { size: 50.0 })),
        material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
        ..default()
    });
    // Cube
    commands.spawn_bundle(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
        material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
        transform: Transform::from_xyz(0.0, 0.5, 0.0),
        ..default()
    });
    // Light
    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..default()
    });
    // Our Sun
    commands
        .spawn_bundle(DirectionalLightBundle {
            directional_light: DirectionalLight {
                shadows_enabled: true,
                ..default()
            },
            ..Default::default()
        })
        .insert(Sun); // Marks the light as Sun

    // Camera
    commands
        .spawn_bundle(Camera3dBundle {
            transform: Transform::from_xyz(-2.0, 1.5, 5.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        })
        .insert(camera::FpsCamera {
            active: false,
            sensitivity: 0.12,
            pitch: 0.0,
            yaw: 0.0,
        });
}
