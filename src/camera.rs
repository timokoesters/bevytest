use bevy::{prelude::*, input::mouse::MouseMotion};

pub struct FpsCameraPlugin;

impl Plugin for FpsCameraPlugin {
	fn build(&self, app: &mut App) {
		app
			.add_system(walk_camera_system)
			.add_system(rotate_camera_system);
	}
}

#[derive(Component)]
pub struct FpsCamera {
    pub active: bool,
    pub yaw: f32,
    pub pitch: f32,
    pub sensitivity: f32,
}

fn rotate_camera_system(mut query: Query<(&mut FpsCamera, &mut Transform)>, mut mouse_motion: EventReader<MouseMotion>) {
    let mut delta: Vec2 = Vec2::ZERO;
	for event in mouse_motion.iter() {
		delta += event.delta;
	}
	if delta.is_nan() {
		return;
	}

    for (mut camera, mut transform) in &mut query {
        if !camera.active {
            return;
        }

        camera.yaw -= delta.x * camera.sensitivity;
        camera.pitch += delta.y * camera.sensitivity;
        camera.pitch = camera.pitch.clamp(-89.0, 89.9);

        //pitch = pitch.clamp(-89.0, 89.9);

        let yaw_radians = camera.yaw.to_radians();
        let pitch_radians = camera.pitch.to_radians();

        transform.rotation = Quat::from_axis_angle(Vec3::Y, yaw_radians)
            * Quat::from_axis_angle(-Vec3::X, pitch_radians);
	}
}

fn walk_camera_system(time: Res<Time>, mut query: Query<(&FpsCamera, &mut Transform)>, keyboard_input: Res<Input<KeyCode>>) {
    for (camera, mut transform) in &mut query {
        if !camera.active {
            return;
        }

        let right = transform.local_x();
        let mut front = -transform.local_z();
        front.y = 0.0;

        if keyboard_input.pressed(KeyCode::D) {
            transform.translation += 4.0 * right * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::A) {
            transform.translation += -4.0 * right * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::W) {
            transform.translation += 4.0 * front * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::S) {
            transform.translation += -4.0 * front * time.delta_seconds();
        }
    }
}

